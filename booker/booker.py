#!/usr/bin/python3
import csv
import datetime
import io
import os
import platform
import smtplib
from email.message import EmailMessage

import psycopg2
import psycopg2.extras
import requests
import yaml

__version__ = "1.8.0"


def connect_chemistry_db():
    settings = get_settings()
    db_conn = psycopg2.connect(
        dsn=f"postgresql://{settings['database_username']}:"
        f"{settings['database_password']}@{settings['database_hostname']}/"
        f"{settings['database_database_name']}",
        cursor_factory=psycopg2.extras.DictCursor,
    )
    return db_conn


def get_settings():
    settings = {}
    base_dir = os.path.dirname(__file__)
    config_filename = "config.yml"
    config_dirname = "booker-tools"
    config_filenames = [
        os.path.join(base_dir, config_filename)
    ]  # highest priority, for devel
    if platform.system() == "Linux":
        config_filenames.append(
            os.path.join(
                os.path.expanduser("~/.config"), config_dirname, config_filename
            )
        )
        config_filenames.append(os.path.join("/etc", config_dirname, config_filename))
    elif platform.system() == "Windows":
        config_filenames.append(
            os.path.join(
                os.environ.get("APPDATA", os.path.expanduser(r"~\AppData\Roaming")),
                config_dirname,
                config_filename,
            )
        )
        config_filenames.append(
            os.path.join(
                os.environ.get("ProgramData", r"C:\ProgramData"),
                config_dirname,
                config_filename,
            )
        )
    config_filenames.append(
        os.path.join(base_dir, "default.yml")
    )  # app defaults, lowest priority
    for f in reversed(config_filenames):
        if os.path.exists(f):
            with open(f, "r") as conf:
                config = yaml.load(conf, Loader=yaml.SafeLoader)
                settings.update(config)
    return settings


def get_booker_creds():
    settings = get_settings()
    return {
        "email": settings["booker_username"],
        "password": settings["booker_password"],
    }


def get_local_people():
    db_conn = connect_chemistry_db()
    cursor = db_conn.cursor()
    cursor.execute("SELECT * FROM apps.booker_users")
    return cursor.fetchall()


def get_resource(resource_path):
    url = base_url + resource_path
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Failed to retrieve resource: {response.status_code} - {response.text}")
        return None


def update_resource(resource_path, json):
    url = base_url + resource_path
    response = requests.put(url, headers=headers, json=json)
    if response.status_code != 200:
        print(f"Failed to update resource: {response.status_code} - {response.text}")
    return response


def add_person(person, dept_id):
    person_data = {
        "Active": True,
        "Username": person["crsid"]
        + "@cam.ac.uk",  # used for oauth2 so should have the domain on it
        "Email": person["email_address"].strip(),
        "Role": "Booker-Room-Requestor",
        "Type": person["booker_type"],
        "ResourceId": person["crsid"],
        "ResourceForename": person["first_names"].strip(),
        "ResourceSurname": person["surname"].strip(),
        "ResourceDepartments": [dept_id],
        "IsOAuth": True,
        "IsPrivileged": False,
        "RoomDisplayPermission": False,
        "IsVisitorManager": False,
        "Credit": 0,
        "CreditSpendingLimit": 0,
        "CanRequestAdditionalServices": False,
        "CanBeBookedByOthers": False,
        "CanBookForOthers": True,
        "CanBookOutsideHours": False,
        "SearchConsent": True,
        "StopWelcomeEmail": True,
        "RoomsManaged": [],
        "DeptsManaged": [],
    }
    response = requests.post(base_url + "/users", headers=headers, json=person_data)
    if response.status_code == 201:
        print(f"{person['crsid']} created successfully")
    elif response.status_code == 409:
        print(f"{person['crsid']} already exists - {response.text}")
        # TODO look up the person's id and add them
    else:
        print(f"Failed to create person: {response.status_code} - {response.text}")
    return response


def login():
    url = base_url + "/token"
    response = requests.post(url, data=get_booker_creds())
    if response.status_code == 200:
        data = response.json()
        headers["Authorization"] = f"Bearer {data.get('Token')}"
    else:
        raise BookerLoginFail(response.text)


def munge_person_for_put(current_person):
    """
    Adjust the person data to match what the update endpoint seems to expect
    """
    role_map = get_booker_roles()
    current_person["Role"] = role_map.get(
        current_person["Role"], "Booker-Room-Requestor"
    )
    current_person["Username"] = current_person["UserName"]
    # Not sure if these are ints or floats, let's be conservative and
    # if we fail to cast to int an exception will be thrown in the Python
    current_person["CreditSpendingLimit"] = int(
        current_person.get("CreditSpendingLimit", 0)
    )
    current_person["Credit"] = int(current_person.get("Credit", 0))
    return current_person


def get_booker_roles():
    roles = get_resource("/roles")
    return {role["DisplayName"]: role["Name"] for role in roles}


def add_person_to_dept(person, dept_id):
    current_person = get_resource(f"/users/{person['AspNetUserId']}")
    current_person["ResourceDepartments"].append(dept_id)
    current_person = munge_person_for_put(current_person)
    print(
        f"Adding person {person['AspNetUserId']} "
        f"{person['ResourceId']} to department {dept_id}"
    )
    response = update_resource(f"/users/{person['AspNetUserId']}", json=current_person)
    return response.status_code == 200


def remove_person_from_dept(person, dept_id):
    current_person = get_resource(f"/users/{person['AspNetUserId']}")
    current_person["ResourceDepartments"].remove(dept_id)
    current_person = munge_person_for_put(current_person)
    print(
        f"Removing person {person['AspNetUserId']} "
        f"{person['ResourceId']} from department {dept_id}"
    )
    response = update_resource(f"/users/{person['AspNetUserId']}", json=current_person)
    return response.status_code == 200


def get_dept_id(dept_name):
    all_departments = get_resource("/departments")
    matching_depts = [
        x for x in all_departments if x.get("Description", "") == dept_name
    ]
    if len(matching_depts) == 1:
        return matching_depts[0].get("OptimeIndex")
    else:
        return None


def get_booker_students():
    return get_resource("/students/lite")


def get_booker_staff():
    return get_resource("/staff/getalllite")


def get_booker_people_crsids(dept_id):
    return [
        removesuffix(x["ResourceId"].lower(), "@cam.ac.uk")
        for x in get_associated_booker_people(dept_id)
    ]


def get_local_crsids():
    return [x["crsid"] for x in get_local_people()]


def get_missing_people(dept_id):
    crsids_already_in_booker = get_booker_people_crsids(dept_id)
    return [x for x in get_local_people() if x["crsid"] not in crsids_already_in_booker]


def get_all_booker_people():
    """
    Get all booker users

    Uses the same API call the GUI does. This one isn't documented so perhaps
    not part of the public API, and I suspect might break. But the alternatives
    are very painful - we have to fetch all staff and all students and then
    look at the students one by one to get their department data  because the
    'get them all' api call doesn't return department data, which we need.
    """
    response = requests.post(
        base_url + "/booker/crud/users",
        headers=headers,
        data={"Draw": 2, "Length": -1, "Start": 0},
    )
    if response.status_code == 200:
        return response.json()["data"]
    else:
        print(
            f"Failed to retrieve Booker people: "
            f"{response.status_code} - {response.text}"
        )
        return []


def get_associated_booker_people(dept_id):
    """
    Get users associated with the given department
    """
    return [x for x in get_all_booker_people() if dept_id in x["ResourceDepartments"]]


# If we had Python 3.9 we could use the string removesuffix method instead
def removesuffix(inp, suffix):
    if inp.endswith(suffix):
        return inp[: -len(suffix)]
    else:
        return inp


def get_people_to_remove(dept_id):
    local_crsids = get_local_crsids()
    settings = get_settings()
    people_not_in_chemistry = [
        x
        for x in get_associated_booker_people(dept_id)
        if x["ResourceId"].lower() not in local_crsids
        and (removesuffix(x["Email"], "@cam.ac.uk")).lower() not in local_crsids
        and x["Email"] != settings["booker_username"]
        and x["Role"] != "Service Manager"
    ]
    return people_not_in_chemistry


def delete_person(booker_person):
    form_data = {
        "AspNetUserId": booker_person["AspNetUserId"],
        "RemoveBookingAction": "Cancel",
    }
    response = requests.post(
        base_url + "/users/delete", data=form_data, headers=headers
    )
    if response.status_code != 200:
        print(f"Failed to delete person: {response.status_code} - {response.text}")
        return False
    return True


def raise_ticket_for_pre_existing_people(pre_existing_people):
    support_email = """
    The automatic sync script was unable to add some Chemistry members to the Booker
    room booking system because they already exist within Booker. Please contact
    roombookingsystem@uis.cam.ac.uk and ask for them to be associated with
    Yusuf Hamied Department of Chemistry as Room Requestors.

    A csv of people who could not be added is attached.


    """
    attachment = io.StringIO("")
    writer = csv.DictWriter(attachment, fieldnames=pre_existing_people[0].keys())
    writer.writeheader()
    writer.writerows(pre_existing_people)
    attachment.seek(0, io.SEEK_SET)
    message = EmailMessage()
    message["Subject"] = "People to add to Booker"
    message["From"] = "root@ch.cam.ac.uk"
    message["To"] = "support@ch.cam.ac.uk"
    message.set_content(support_email)
    message.add_attachment(attachment.read(), filename="booker.csv")
    s = smtplib.SMTP("localhost")
    s.send_message(message)
    s.quit()


def get_person_datefield(booker_person, fieldname):
    datefield = None
    try:
        # truncates string to remove microseconds completely
        # as we have records with more than 6 digits
        datefield = datetime.datetime.strptime(
            booker_person.get(fieldname, None)[:19], "%Y-%m-%dT%H:%M:%S"
        )
    except TypeError:
        pass
    return datefield


def get_person_last_logintime(booker_person):
    return get_person_datefield(booker_person, "LastLogin")


def get_logged_in_person():
    return get_resource("/users")


def get_logged_in_username():
    return get_logged_in_person()["UserName"]


class BookerLoginFail(Exception):
    pass


base_url = get_settings()["booker_api_base_url"]
headers = {}
